import math
def calc_parameter(dt,x,A_pass,A_out,A_in,T_out,P_out,rho_out,h_out,T_r,P_r,rho_r,h_r,R_out,R_in,R_pass,x_out,x_in,x_pass,x_pist,L_tot):
    if x < 0:
        return
    elif x + x_pist < x_pass:
       A_ref = A_pass
       P_ref = P_r
       rho_ref = rho_r
       h_ref = h_r
       T_ref=T_r
    elif x + x_pist < x_pass + 2*R_pass/L_tot:
        a = (x - (x_pass - x_pist))*L_tot
        if a <= R_pass:
            alpha = 2*math.acos((R_pass - a)/R_pass)
            A_ref = 4*(math.pi*R_pass**2 - 1/2* R_pass**2 * (alpha - math.sin(alpha)))
        else:
            a = 2*R_pass - a
            alpha = 2*math.acos((R_pass - a)/R_pass)
            A_ref = 4*(1/2* R_pass**2 * (alpha - math.sin(alpha)))
        P_ref = P_r
        rho_ref = rho_r
        h_ref = h_r
        T_ref=T_r      
    elif x < x_out:
        A_ref = 0
        P_ref = P_r
        rho_ref = rho_r
        h_ref = h_r
        T_ref=T_r
    elif x < x_out + 2*R_out/L_tot:
        a = (x-x_out)*L_tot
        if a <= R_out:
            alpha = 2*math.acos((R_out - a)/R_out)
            A_ref =2*( 1/2 *R_out**2 * (alpha - math.sin(alpha)))
        else:
            a = 2*R_out - a
            alpha = 2*math.acos((R_out - a)/R_out)
            A_ref =2*(math.pi*R_out**2 - 1/2* R_out**2 * (alpha - math.sin(alpha)))
        P_ref = P_out
        rho_ref = rho_out
        h_ref = h_out
        T_ref = T_out
    else:
        A_ref = A_out
        P_ref = P_out
        rho_ref = rho_out
        h_ref = h_out
        T_ref = T_out
    if x + x_pist < x_in:
        A_in_ref = A_in
    elif x + x_pist < x_in + 2*R_in/L_tot:
        a = (x-(x_in - x_pist))*L_tot
        if a <= R_in:
            alpha = 2*math.acos((R_in - a)/R_in)
            A_in_ref = 1/2 *R_in**2 * (alpha - math.sin(alpha))
        else:
            a = 2*R_in - a
            alpha = 2*math.acos((R_in - a)/R_in)
            A_in_ref = math.pi*R_in**2 - 1/2* R_in**2 * (alpha - math.sin(alpha))
    else:
        A_in_ref = 0
    return A_ref,P_ref,rho_ref,h_ref,T_ref,A_in_ref