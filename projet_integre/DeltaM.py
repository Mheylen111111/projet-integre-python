import CoolProp
from CoolProp.CoolProp import PropsSI
import numpy as np
from CoolProp.Plots import PropertyPlot
from matplotlib import pyplot as plt
import math
from scipy import optimize
def gamma_fct(x,P_ex,P_su,s_su,rho_su,fluid):
    gamma = max(0.1,min(x,3))
    P_thr = max(P_ex,P_su * (2/(gamma+1))**(gamma/(gamma-1)))
    rho_thr = PropsSI('D','P',P_thr,'S',s_su,fluid)
    gamma_bis = math.log10(P_su/P_thr)/math.log10(rho_su/rho_thr)
    return gamma_bis - gamma
###################################################
def P_critique(P,T,P2,fluid,type_tuyere,type_gaz):
    if type_tuyere==0:
        cp=PropsSI('CPMASS','P',P,'T',T,fluid)
        cv=PropsSI('CVMASS','P',P,'T',T,fluid)
        gamma = cp/cv
        P_crit=(2/(gamma+1))**(gamma/(gamma-1))*P
    elif type_tuyere == 1:
        s= PropsSI('S','T', T, 'P',P,fluid)
        rho = PropsSI('D','T', T, 'P',P,fluid)
        lb = 0.1
        ub = 2.
        gamma = optimize.brentq(gamma_fct,lb,ub,args=(P2,P,s,rho,fluid))
        P_crit=(2/(gamma+1))**(gamma/(gamma-1))*P
    else:
        P_crit = 0
    return P_crit
###################################################
def DeltaM_main(T_ref,P_ref,rho_ref,h_ref,T,P,rho,h,A,dt,fluid,type_tuyere,type_gaz,delta_m,mdelta_h,i):
    if P_ref > P:
        P_crit = P_critique(P_ref,T_ref,P,fluid,type_tuyere,type_gaz)
        m_dot = A * math.sqrt(2* rho_ref *(P_ref-max(P_crit,P)))
        delta_m.append(m_dot * dt)
        mdelta_h.append(delta_m[i] * h_ref)
    elif P_ref < P:
        P_crit = P_critique(P,T,P_ref,fluid,type_tuyere,type_gaz)
        m_dot = A * math.sqrt(2 * rho *(P - max(P_crit,P_ref)))
        delta_m.append(-m_dot * dt)
        mdelta_h.append(delta_m[i] * h)
    else:
        delta_m.append(0)
        mdelta_h.append(0)
    return delta_m,mdelta_h   
###################################################
def DeltaM_leak(A_in,A_out,A_pass,x,T_su,P_su,rho_su,h_su,T_ex,P_ex,rho_ex,h_ex,T_l,P_l,rho_l,h_l,T_r,P_r,rho_r,h_r,T_above,P_above,rho_above,h_above,A_leak,dt,R_in,R_out,x_out,x_in,x_pist,L_tot,fluid,type_tuyere,type_gaz,delta_m_ex_l,delta_m_l_r,delta_m_r_ex,delta_m_su_l,delta_m_r_su,delta_m_above_r,mdelta_h_ex_l,mdelta_h_l_r,mdelta_h_r_ex,mdelta_h_su_l,mdelta_h_r_su,mdelta_h_above_r,i):
    if x < x_out:
        ######################################################
        #  |_________|  # Piston au dessus de la sortie      #
        #   ___   ___   # Du fluide peut circuler en dessous #
        ###################################################### 
        P=[P_ex,P_l,P_r]
        m_dot = [0,0,0]
        delta_h = [0,0,0]
        rho=[rho_ex,rho_l,rho_r]
        h=[h_ex,h_l,h_r]
        T=[T_ex,T_l,T_r]
        if P[0]>P[1]: # Du fluide va aller de la sortie vers la gauche
            P_crit = P_critique(P[0],T[0],P[1],fluid,type_tuyere,type_gaz)
            m_dot[0]=1/(1/A_out+1/A_leak) * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[1])))
            delta_h[0] = h[0]
        else:
            P_crit = P_critique(P[1],T[1],P[0],fluid,type_tuyere,type_gaz)
            m_dot[0]=-1/(1/A_out+1/A_leak) * math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[0])))
            delta_h[0] = h[1]

        if P[1]>P[2]: #Du fluide va aller de la gauche vers la droite
            P_crit = P_critique(P[1],T[1],P[2],fluid,type_tuyere,type_gaz)
            m_dot[1]=(A_leak + 1/(1/A_leak+1/A_pass))* math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[2])))
            delta_h[1] = h[1]
        else:
            P_crit = P_critique(P[2],T[2],P[1],fluid,type_tuyere,type_gaz)
            m_dot[1]=-(A_leak + 1/(1/A_leak+1/A_pass)) * math.sqrt(2* rho[2] *(P[2]-max(P_crit,P[1])))
            delta_h[1] = h[2]
        
        if P[2]>P[0]: #Du fluide va aller de la droite vers la sortie
            P_crit = P_critique(P[2],T[2],P[0],fluid,type_tuyere,type_gaz)
            m_dot[2]=1/(1/A_out+1/A_leak) * math.sqrt(2* rho[2] *(P[2]-max(P_crit,P[0])))
            delta_h[2] = h[2]
        else:
            P_crit = P_critique(P[0],T[0],P[2],fluid,type_tuyere,type_gaz)
            m_dot[2]=-1/(1/A_out+1/A_leak) * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[2])))
            delta_h[2] = h[0]
        
        delta_m_ex_l.append(m_dot[0] * dt)
        delta_m_l_r.append(m_dot[1] * dt)
        delta_m_r_ex.append(m_dot[2] * dt)
        delta_m_su_l.append(0)
        delta_m_r_su.append(0)
           
        mdelta_h_ex_l.append(delta_m_ex_l[i] * delta_h[0])
        mdelta_h_l_r.append(delta_m_l_r[i] * delta_h[1])
        mdelta_h_r_ex.append(delta_m_r_ex[i] * delta_h[2])
        mdelta_h_su_l.append(0)
        mdelta_h_r_su.append(0)
           
    elif x < x_out + 2*R_out/L_tot:
        P = [P_l,P_r]
        rho=[rho_l,rho_r]
        h=[h_l,h_r]
        T=[T_l,T_r]
        if P[0]>P[1]: #Du fluide va aller de la gauche vers la droite
            P_crit = P_critique(P[0],T[0],P[1],fluid,type_tuyere,type_gaz)
            m_dot=(A_leak + 1/(1/A_leak+1/A_pass)) * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[1])))   
            delta_h = h[0]
        else:
            P_crit = P_critique(P[1],T[1],P[0],fluid,type_tuyere,type_gaz)
            m_dot=-(A_leak + 1/(1/A_leak+1/A_pass)) * math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[0])))
            delta_h = h[1]
   
        delta_m_ex_l.append(0)
        delta_m_l_r.append(m_dot * dt)
        delta_m_r_ex.append(0)
        delta_m_su_l.append(0)
        delta_m_r_su.append(0)
           
        mdelta_h_ex_l.append(0)
        mdelta_h_l_r.append(delta_m_l_r[i] * delta_h)
        mdelta_h_r_ex.append(0)
        mdelta_h_su_l.append(0)
        mdelta_h_r_su.append(0)
           
    elif x >= x_in - x_pist + 2*R_in/L_tot:
    ######################################################
    #  |_________|  # Piston au dessus de l'entrée       #
    #   ___   ___   # Du fluide peut circuler en dessous #
    ######################################################
        P=[P_su,P_l,P_r]
        m_dot = [0,0,0]
        delta_h = [0,0,0]
        rho=[rho_su,rho_l,rho_r]
        h=[h_su,h_l,h_r]
        T=[T_su,T_l,T_r]
        if P[0]>P[1]: #Du fluide va aller de l'entrée vers la gauche
            P_crit = P_critique(P[0],T[0],P[1],fluid,type_tuyere,type_gaz)
            m_dot[0]=1/(1/A_in+1/A_leak) * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[1])))    
            delta_h[0] = h[0]
        else:
            P_crit = P_critique(P[1],T[1],P[0],fluid,type_tuyere,type_gaz)
            m_dot[0]=-1/(1/A_in+1/A_leak)* math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[0])))
            delta_h[0] = h[1]
        
        if P[1]>P[2]: #Du fluide va aller de la gauche vers la droite
            P_crit = P_critique(P[1],T[1],P[2],fluid,type_tuyere,type_gaz)
            m_dot[1]=A_leak * math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[2])))
            delta_h[1] = h[1]
        else:
            P_crit = P_critique(P[2],T[2],P[1],fluid,type_tuyere,type_gaz)
            m_dot[1]=-A_leak * math.sqrt(2* rho[2] *(P[2]-max(P_crit,P[1])))
            delta_h[1] = h[2]
        
        if P[2]>P[0]: #Du fluide va de la droite vers l'entrée
            P_crit = P_critique(P[2],T[2],P[0],fluid,type_tuyere,type_gaz)
            m_dot[2]=1/(1/A_in+1/A_leak) * math.sqrt(2* rho[2] *(P[2]-max(P_crit,P[0])))
            delta_h[2] = h[2]
        else:
            P_crit = P_critique(P[0],T[0],P[2],fluid,type_tuyere,type_gaz)
            m_dot[2]=-1/(1/A_in+1/A_leak) * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[2])))
            delta_h[2] = h[0]
        
        delta_m_ex_l.append(0)
        delta_m_l_r.append(m_dot[1] * dt)
        delta_m_r_ex.append(0)
        delta_m_su_l.append(m_dot[0] * dt)
        delta_m_r_su.append(m_dot[2] * dt)
           
        mdelta_h_ex_l.append(0)
        mdelta_h_l_r.append(delta_m_l_r[i] * delta_h[1])
        mdelta_h_r_ex .append(0)
        mdelta_h_su_l.append(delta_m_su_l[i] * delta_h[0])
        mdelta_h_r_su.append(delta_m_r_su[i] * delta_h[2])
           
    else:
    ######################################################
    #  |_________|  # Piston au dessus de la paroi       #
    #   _________   # Du fluide peut circuler en dessous #
    ######################################################    
        P=[P_l,P_r]
        rho=[rho_l,rho_r]
        h=[h_l,h_r]
        T=[T_l,T_r]
        if P[0]>P[1]: #Du fluide va aller de la gauche vers la droite
            P_crit = P_critique(P[0],T[0],P[1],fluid,type_tuyere,type_gaz)
            m_dot=A_leak * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[1])))  
            delta_h = h[0]
        else:
            P_crit = P_critique(P[1],T[1],P[0],fluid,type_tuyere,type_gaz)
            m_dot=-A_leak * math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[0])))
            delta_h = h[1]
        
        delta_m_ex_l.append(0)
        delta_m_l_r.append(m_dot*dt)
        delta_m_r_ex.append(0)
        delta_m_su_l.append(0)
        delta_m_r_su.append(0)
           
        mdelta_h_ex_l.append(0)
        mdelta_h_l_r.append(delta_m_l_r[i] * delta_h)
        mdelta_h_r_ex.append(0)
        mdelta_h_su_l.append(0)
        mdelta_h_r_su.append(0)
  
        P=[P_above,P_r]
        rho=[rho_above,rho_r]
        h=[h_above,h_r]
        T=[T_above,T_r]
        if P[0]>P[1]: #Du fluide va aller de la gauche vers la droite
            P_crit = P_critique(P[0],T[0],P[1],fluid,type_tuyere,type_gaz)
            m_dot=A_leak * math.sqrt(2* rho[0] *(P[0]-max(P_crit,P[1])))  
            delta_h = h[0]
        else:
            P_crit = P_critique(P[1],T[1],P[0],fluid,type_tuyere,type_gaz)
            m_dot=-A_leak * math.sqrt(2* rho[1] *(P[1]-max(P_crit,P[0])))
            delta_h = h[1]
        
        delta_m_above_r.append(m_dot * dt)
        mdelta_h_above_r.append(delta_m_above_r[i] * delta_h)
    return delta_m_ex_l,delta_m_l_r,delta_m_r_ex,delta_m_su_l,delta_m_r_su,delta_m_above_r,mdelta_h_ex_l,mdelta_h_l_r,mdelta_h_r_ex,mdelta_h_su_l,mdelta_h_r_su,mdelta_h_above_r