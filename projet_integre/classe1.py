



###############################################################################
"""Initialized_vector"""
from initialized_vector import initialized_vector
delta_m_l,mdelta_h_l,delta_m_ex,mdelta_h_ex,delta_m_ext,mdelta_h_ext = initialized_vector()
###############################################################################
"""Main code"""
from calc_parameter import calc_parameter
from DeltaM import DeltaM_main
error = 0
stop = 0
compteur = 0
i = 1

while True:
    if i!=1:
        if compteur == 0: 
                break 
    A_ref,P_ref,rho_ref,h_ref,T_ref,A_in_ref = calc_parameter(dt[i-1],x[i-1],A_pass,A_out,A_in,T_out,P_out,rho_out,h_out,T_r[i-1],P_r[i-1],rho_r[i-1],h_r[i-1],R_out,R_in,R_pass,x_out,x_in,x_pass,x_pist,L_tot)
    delta_m_l,mdelta_h_l = DeltaM_main(T_ref,P_ref,rho_ref,h_ref,T_l[i-1],P_l[i-1],rho_l[i-1],h_l[i-1],A_ref,dt[i-1],fluid,type_tuyere,type_gaz,delta_m_l,mdelta_h_l,i)   
    if P_ref==P_out:
        delta_m_ex[i] = -delta_m_l[i]
        mdelta_h_ex[i] = -mdelta_h_l[i]
    else:
        delta_m_ex[i] = 0
        mdelta_h_ex[i] = 0
    delta_m_ext,mdelta_h_ext = DeltaM_main(T_out,P_out,rho_out,h_out,T_ext[i-1],P_ext[i-1],rho_ext[i-1],h_ext[i-1],A_ref,dt[i-1],fluid,type_tuyere,type_gaz,delta_m_ext,mdelta_h_ext,i)   
    delta_m_su,mdelta_h_su = DeltaM_main(T_in,P_in,rho_in,h_in,T_r[i-1],P_r[i-1],rho_r[i-1],h_r[i-1],A_ref,dt[i-1],fluid,type_tuyere,type_gaz,delta_m_su,mdelta_h_su,i)   
    
    i +=1