import sys
import os.path

import CoolProp
from CoolProp.CoolProp import PropsSI
import numpy as np
from CoolProp.Plots import PropertyPlot
from matplotlib import pyplot as plt
import math
###############################################################################
"""Import Datas"""
from import_Data import *
Data = DATA(1,0,1,'air',7e5,50,1e5,20,10**-6,10**-5)
###############################################################################
"""Initialized_vector"""
from initialized_vector import *
Vector = INIT_vector()
###############################################################################
"""Main code"""
from calc_parameter import *
from DeltaM import *
error = 0
stop = 0
compteur = 0
i = 1
while True:
    if i!=1:
        if compteur == 0: 
                break 
    A_ref,P_ref,rho_ref,h_ref,T_ref,A_in_ref = calc_parameter(Data.dt[i-1],Data.x[i-1],Data.A_pass,Data.A_out,Data.A_in,Data.T_out,Data.P_out,Data.rho_out,Data.h_out,Data.T_r[i-1],Data.P_r[i-1],Data.rho_r[i-1],Data.h_r[i-1],Data.R_out,Data.R_in,Data.R_pass,Data.x_out,Data.x_in,Data.x_pass,Data.x_pist,Data.L_tot)  
    Vector.delta_m_l,Vector.mdelta_h_l = DeltaM_main(T_ref,P_ref,rho_ref,h_ref,Data.T_l[i-1],Data.P_l[i-1],Data.rho_l[i-1],Data.h_l[i-1],A_ref,Data.dt[i-1],Data.fluid,Data.type_tuyere,Data.type_gaz,Vector.delta_m_l,Vector.mdelta_h_l,i)   
    if P_ref==Data.P_out:
        Vector.delta_m_out[i] = -Vector.delta_m_l[i]
        Vector.mdelta_h_out[i] = -Vector.mdelta_h_l[i]
    else:
        Vector.delta_m_out[i] = 0
        Vector.mdelta_h_out[i] = 0
    Vector.delta_m_ext,Vector.mdelta_h_ext = DeltaM_main(Data.T_out,Data.P_out,Data.rho_out,Data.h_out,Data.T_ext[i-1],Data.P_ext[i-1],Data.rho_ext[i-1],Data.h_ext[i-1],A_ref,Data.dt[i-1],Data.fluid,Data.type_tuyere,Data.type_gaz,Vector.delta_m_ext,Vector.mdelta_h_ext,i)   
    Vector.delta_m_su,Vector.mdelta_h_su = DeltaM_main(Data.T_in,Data.P_in,Data.rho_in,Data.h_in,Data.T_r[i-1],Data.P_r[i-1],Data.rho_r[i-1],Data.h_r[i-1],A_ref,Data.dt[i-1],Data.fluid,Data.type_tuyere,Data.type_gaz,Vector.delta_m_su,Vector.mdelta_h_su,i)   
    Vector.delta_m_ex_l_leak,Vector.delta_m_l_r_leak,Vector.delta_m_r_ex_leak,Vector.delta_m_su_l_leak,Vector.delta_m_r_su_leak,Vector.delta_m_above_r_leak,Vector.mdelta_h_ex_l_leak,Vector.mdelta_h_l_r_leak,Vector.mdelta_h_r_ex_leak,Vector.mdelta_h_su_l_leak,Vector.mdelta_h_r_su_leak,Vector.mdelta_h_above_r_leak = DeltaM_leak(Data.A_in,Data.A_out,Data.A_pass,Data.x[i-1],Data.T_in,Data.P_in,Data.rho_in,Data.h_in,Data.T_out,Data.P_out,Data.rho_out,Data.h_out,Data.T_l[i-1],Data.P_l[i-1],Data.rho_l[i-1],Data.h_l[i-1],Data.T_r[i-1],Data.P_r[i-1],Data.rho_r[i-1],Data.h_r[i-1],Data.T_ext[i-1],Data.P_ext[i-1],Data.rho_ext[i-1],Data.h_ext[i-1],Data.A_leak,Data.dt[i-1],Data.R_in,Data.R_out,Data.x_out,Data.x_in,Data.x_pist,Data.L_tot,Data.fluid,Data.type_tuyere,Data.type_gaz,Vector.delta_m_ex_l_leak,Vector.delta_m_l_r_leak,Vector.delta_m_r_ex_leak,Vector.delta_m_su_l_leak,Vector.delta_m_r_su_leak,Vector.delta_m_above_r_leak,Vector.mdelta_h_ex_l_leak,Vector.mdelta_h_l_r_leak,Vector.mdelta_h_r_ex_leak,Vector.mdelta_h_su_l_leak,Vector.mdelta_h_r_su_leak,Vector.mdelta_h_above_r_leak,i)

    Data.m_l[i] = Data.m_l[i-1] + Vector.delta_m_l[i] + Vector.delta_m_ex_l_leak[i] + Vector.delta_m_su_l_leak[i] - Vector.delta_m_l_r_leak[i]
    Vector.mdelta_h_l_tot[i] = Vector.mdelta_h_l[i] + Vector.mdelta_h_ex_l_leak[i] - Vector.mdelta_h_l_r_leak[i] + Vector.mdelta_h_su_l_leak[i]

    i +=1