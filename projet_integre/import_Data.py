from CoolProp.CoolProp import PropsSI
import math
class DATA: 
    def __init__(self,X,Y,Z,fluid,P_in,T_in_C,P_out,T_out_C,min_dt,max_dt):
        self.type_gaz = X #1: gaz parfait;  2: gaz reel
        self.type_tuyere = Y #0:gaz parfait; 1:gaz reel - gamma fictif
        self.type_piston = Z #1: Laiton; 2: ABS
    ###############################################################################
        if self.type_piston == 1:
            self.D_o = 0.028 #Diametre du cylindre du piston 
            self.D_i = self.D_o - 1e-6
            self.D_arbre = 0.027 #Diametre de l'arbre
            self.D_aminci = 0.02
            self.L_tot = 0.056 #Longueur totale
            self.th_pist = 0.0255 #epaisseur du piston
            self.m_pist = 221.7e-3 #masse du piston
        else:
            self.D_o = 0.028
            self.D_i = D_o - 1e-6
            self.D_arbre = 0.027 #Diametre de l'arbre
            self.D_aminci = 0.027
            self.L_tot = 0.056
            self.th_pist = 0.0255 #epaisseur du piston
            self.m_pist = 34.8e-3
        self.e=0.005 #epaisseur du piston
        self.L_arbre = 0.023
    ###############################################################################
        self.x_pist = self.th_pist/self.L_tot
        self.x_pass = 0.0265/self.L_tot
        self.x_out = 0.0147/self.L_tot #Proportion de la longueur entre le bord "gauche" et la sortie
        self.x_in = 0.046/self.L_tot ##Proportion de la longueur entre le bord "gauche" et l'entree
        ###############################################################################
        self.L_out = self.x_out*self.L_tot #Distance entre l'extremite et le debut de la sortie
        self.L_in = self.x_in*self.L_tot #Distance entre l'extremite et le debut de l'entree
        self.R_in = 0.004 #Rayon de l'entree
        self.R_out = 0.0016 #Rayon de la sortie
        self.R_pass = 0.002 #Rayon du passage
        self.R_above = self.R_in
    ###############################################################################
        self.A_above = 2 * math.pi*self.R_above**2
        self.A_in = math.pi*self.R_in**2 #Aire de sortie du fluide 
        self.n_sortie = 2 #Nombre de sorties
        self.n_pass = 4 #Nombre de passages
        self.A_out = self.n_sortie*math.pi*self.R_out**2 #Aire d'entree du fluide
        self.A_pass = self.n_pass*math.pi*self.R_pass**2 #A_out/n_sortie #Aire de passage entre les deux chambres
        self.A_r = math.pi * (self.D_i**2 - self.D_arbre**2)/4 #Aire d'application de la pression du cote droit
        self.A_l = math.pi * self.D_i**2/4 #Aire d'application de la pression a gauche
        self.A_leak = math.pi * (self.D_o**2 - self.D_i**2)/4 #Aire de fuite entre le piston et le cylindre
        self.Vol_pist = math.pi * self.D_i**2/4 * self.th_pist #Volume du piston (il faut rajouter aussi celui de l'arbre)
    ###############################################################################
        self.fluid = fluid
        self.M =  PropsSI('M','T',297.15,'P',101325,fluid)
        self.R = 8.314
        self.r = self.R/self.M
        self.T_amb = 21+273.15
    ###############################################################################
        self.P_in = P_in
        self.T_in = T_in_C + 273.15
        self.h_in = PropsSI('H','T',self.T_in,'P',P_in,fluid)   # [J/kg]
        self.rho_in = PropsSI('D','T',self.T_in,'P',P_in,fluid) # [kg/m3]
        self.v_in = 1/self.rho_in
        self.u_in = PropsSI('U','T',self.T_in,'P',P_in,fluid)   # [J/kg] 
        self.s_in = PropsSI('S','T',self.T_in,'P',P_in,fluid)
    ###############################################################################   
        self.P_out = P_out
        self.T_out = T_out_C + 273.15
        self.h_out = PropsSI('H','T',self.T_out,'P',P_out,fluid)   #enthalpie specifique
        self.rho_out = PropsSI('D','T',self.T_out,'P',P_out,fluid) #Masse volumique
        self.u_out = PropsSI('U','T',self.T_out,'P',P_out,fluid)   #energie interne specifique
        self.s_out= PropsSI('S','T',self.T_out,'P',P_out,fluid)
        #On prend x la position specifique de la partie gauche du piston
    ###############################################################################
        #On considere le cote gauche comme le cote ou se deroule la detente. On
        #donne donc les valeurs de sortie pour commencer
        self.x = [0.1]                                            #Par convention on prendra le x pour la partie gauche du piston
        self.P_l = [P_out]                                             #Pression
        self.T_l = [self.T_out]                                             #Hypothese de depart sinon il manque une donnee (a voir plus tard)
        self.h_l = [self.h_out]                                             #enthalpie
        self.u_l = [self.u_out]                                             #energie interne
        self.s_l = [self.s_out]                                             #entropie
        self.rho_l = [self.rho_out]                                         #masse volumique
        self.v_l = [1/self.rho_l[0]]                                          #volume specifique
        self.V_l = [math.pi * self.D_o**2/4 * (self.x[0] * self.L_tot)]                 #On calcul le volume et on oublie pas la partie prise par le piston
        self.m_l = [self.rho_l[0] * self.V_l[0]]                                      #masse d'air
        self.F_l = [self.P_l[0] * self.A_l]                                        #Force applique
    ###############################################################################    
        #Le cote droite est la cote en lien avec l'entree on met donc les valeurs
        #egale a celle du supply pour commencer
        self.P_r = [P_in]                                             #Pression
        self.T_r = [self.T_in]                                             #Hypothese de depart sinon il manque une donnee (a voir plus tard)
        self.h_r = [self.h_in]                                             #enthalpie
        self.u_r = [self.u_in]                                             #energie interne
        self.s_r = [self.s_in]                                             #entropie
        self.rho_r = [self.rho_in]                                         #masse volumique
        self.v_r = [1/self.rho_r[0]]                                          #volume specifique
        if self.type_piston == 2:
            self.V_r = [math.pi*(self.D_o**2-self.D_arbre**2)/4*((1-self.x[0]-self.x_pist)*self.L_tot)]
        else:
            self.V_r = [math.pi*(self.D_o**2-self.D_arbre**2)/4*((1-self.x[0]-self.x_pist)*self.L_tot) + math.pi*((self.D_arbre**2 - self.D_aminci**2)/4)*self.L_arbre] #On calcul le volume et on oublie pas la partie prise par le piston    
        self.m_r = [self.rho_r[0] * self.V_r[0]]     #masse d'air
        self.F_r = [- self.P_r[0] * self.A_r]     #Force appliquee au piston. Le signe - vient de la convention des axes 
    ###############################################################################   
        self.P_ext = [P_out]                                             #Pression
        self.T_ext = [self.T_amb]                                            #Hypothese de depart sinon il manque une donnee (a voir plus tard)
        self.h_ext = [self.h_out]                                             #enthalpie
        self.u_ext = [self.u_out]                                             #energie interne
        self.s_ext = [self.s_out]                                             #entropie
        self.rho_ext = [self.rho_out]                                         #masse volumique
        self.v_ext = [1/self.rho_ext[0]]                                      #volume specifique
        self.V_ext = math.pi * self.D_arbre**2/4 * (self.L_tot-self.x[0] * self.L_tot)       #On calcul le volume et on oublie pas la partie prise par le piston
        self.m_ext = [self.rho_ext[0] * self.V_ext]                             #masse d'air
        self.F_ext = [- P_out * (self.A_l-self.A_r)]        
    ###############################################################################
        a = 2 * (min_dt - max_dt)/(self.L_tot**2/2)
        b =4 * (max_dt -min_dt)/self.L_tot
        c = min_dt
        self.dt = [a*self.x[0]**2+b*self.x[0]+c] #Pas de temps entre iteration (dt=10**-4 en x=0.3 et dt=10**-6 en x=0 et x=0.6)
        self.T = 0 #On regarde le temps pour voir plus tard la frequence
    ###############################################################################
        self.F_res = [self.F_r[0] + self.F_l[0] + self.F_ext[0]]                            #Force resultante sur le piston
        self.a_pist = [self.F_res[0]/self.m_pist]                                  #Acceleration du piston
        self.V_pist = [self.a_pist[0]*self.dt[0]]                                     #Vitesse du piston