# -*- coding: utf-8 -*-
"""
Created on Mon Sep  9 11:19:37 2019

@author: marti
"""
from CoolProp.CoolProp import PropsSI

CP = PropsSI('CPMASS','P',100000,'T',300,'CH4')
S = PropsSI('S','P',100000,'T',300,'CH4')
T = PropsSI('T','P',300000,'S',S,'CH4')
D = PropsSI('D','P',100000,'T',300,'CH4')
m_dot = 240./(60*1000) * D
W_dot = m_dot * CP * (T-300)